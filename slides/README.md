---
title: CI / CD
verticalSeparator: ^----$
---

# CI / CD

---

## Disclaimer

note:
    > je ne suis pas Charles Xavier / Professor X : je ne suis pas télépathe
    >
    > donc si vous ne dites pas que vous ne comprennez pas, je ne peux pas le savoir
    >
    > il n'y a aucune questions stupides
    >
    > si vous ne dites pas que vous ne savez pas, vous n'apprendrez pas
    >
    > n'hésitez pas à demander tant que vous n'avez pas compris

---

## Tour de table

* votre niveau de connaissance
  * honnete
  * sans jugement
* vos attentes

---

### Objectifs

<ul>
<li class="fragment">

Montrer qu'un système de CI / CD __peut__ (et _devrait_) __etre simple et facile__ à mettre en place et faire évoluer

<li class="fragment">

__Démystifier__ / désacraliser

<li class="fragment">

__Inciter__ à mettre en place et faire évoluer une CI / CD

<li class="fragment">

Montrer que la CI __évolue organiquement__
</ul>

---

## Théorie

----

Quel est le but d'une équipe de développement ?

Apporter de la valeur <!-- .element: class="fragment" -->

----

Comment une équipe de développement apporte de la valeur ?

En exécutant en production un programme qu'elle a créé qui permet d'automatiser des tâches <!-- .element: class="fragment" -->

note:
    Créer un programme (en écrivant du code) n'est qu'un __investissement__ qui ne rapporte pas de valeur, tant qu'il n'a pas été mit en production

----

Mettre en production régulièrement

=

Apporter de la valeur régulièrement

[Software Crafter Manifesto](https://manifesto.softwarecraftsmanship.org)

----

### Du code à la prod

Quelles sont les différentes étapes ?

Quels sont les outils ?

note:
    Faire deviner les différentes étapes du cycle de vie de l'application, entre le moment où on écrit une ligne de code et le moment où ça apporte de la valeur

----

|      | Étape                 | Responsabilité |
| :--- | :-------------------- | :------------- |
|      | Code                  | Source         |
| CI   | Check                 | Qualité        |
| CD   | Build                 | Livrable       |
| CD   | Deploy                | Env            |
|      | Run                   | Valeur         |
|      | Monitoring / Alerting | Valeur         |

note:
    Présentation cycle de vie de l'application
    Expliquer le role de chaque étapes
    Préciser que ce __n'est qu'une__ façon de voir ce cycle
    Liste non exaustive d'outils permettant de gérer :
    [Continuous Integration](https://landscape.cncf.io/?group=projects-and-products&view-mode=card#app-definition-and-development--continuous-integration-delivery)

----

### Définitions

<ul>
    <li class="fragment">CI
        <ul>
            <li class="fragment">Continuous Integration / Intégration Continue
        </ul>
    <li class="fragment">CD
        <ul>
            <li class="fragment">⚠️ CD ≠ CD
                <ul>
                    <li class="fragment">Continuous Delivery / Livraison Continue
                    <li class="fragment">Continuous Deployment / Déploiement Continue
                </ul>
        </ul>
</ul>

----

### Comment commencer ?

<ol>
    <li class="fragment">Écrire la procédure
    <li class="fragment">Scripter
    <li class="fragment">Automatiser
        <ol>
            <li>CI
            <li>CD
            <li>CD
        </ol>
    <li class="fragment">Améliorer en continue
</ol>

----

### Réduire la boucle de feedback

[![eXtreme Programming feedback loops](xp_feedback_loops.png)](http://www.extremeprogramming.org/map/loops.html)

[eXtreme Programming feedback loops](http://www.extremeprogramming.org/map/loops.html)

---

## Pause

5min

---

## Pratique : démo GitLab CI

----

1. expliquer le besoin / exercice

   > un programme qui dit "bonjour" aux gens

1. setup l'env en local

   ```sh
   echo "pytest~=6.0" >> requirements.txt
   pip3 install --requirement requirements.txt
   ```

1. écrire le programme

   `hello.py`

   ```python
   def hello(name):
       return f'Hello {name}'

   def test_hello_world():
       assert hello('world') == 'Hello world'

   def test_hello_toto():
       assert hello('toto') == 'Hello toto'
   ```

1. tester en local

   ```sh
   pytest hello.py
   ```

1. créer le repo local

   ```sh
   git init
   git add .
   git commit
   ```

1. créer le repo [sur GitLab](https://gitlab.com/projects/new) et ajouter la remote

   ```sh
   git remote add origin git@gitlab.com:USER/REPO.git
   ```

1. décrire la pipeline

   `.gitlab-ci.yml`

   ```yaml
   test:
       image: python:3-alpine
       script:
           - pip3 install --requirement requirements.txt
           - pytest hello.py
   ```

1. lancer la pipeline `git push origin main`
1. montrer que la pipeline fonctionne
1. expliquer `job` > `script` > `command`
1. compléter la pipeline avec le déploiement en utilisant `GitLab Pages`: serveur de fichiers statiques gratuit

   ```sh
   echo '<a href="hello.py">hello</a>' >> index.html
   ```

   `.gitlab-ci.yml`

   ```yaml
   test:
       image: python:3-alpine
       script:
           - pip3 install --requirement requirements.txt
           - pytest hello.py

   pages:
       script:
           - mkdir public
           - mv index.html hello.py public
       artifacts:
           paths:
               - public/
           expire_in: 1 week
   ```

1. aller sur la page
    `https://USER.gitlab.io/REPO/index.html`
1. télécharger le fichier
1. ajouter un test qui fail
1. montrer que la CI fail
1. montrer que le fichier téléchargé est mis à jour malgrès le problème
1. corriger le test
1. montrer que le fichier téléchargé est mis à jour
1. ajouter différents `stages`

   `.gitlab-ci.yml`

   ```yaml
   stages:
       - test
       - deploy

   test:
       stage: test
       image: python:3-alpine
       script:
           - pip3 install --requirement requirements.txt
           - pytest hello.py

   pages:
       stage: deploy
       script:
           - mkdir public
           - mv index.html hello.py public
       artifacts:
           paths:
               - public/
           expire_in: 1 week
   ```

1. montrer la pipeline multi-`stages`
1. expliquer `pipeline` > `stage` > `job` > `script` > `command`
1. ajouter un test qui fail
1. montrer que la pipeline fail
1. montrer que le fichier téléchargé est resté sur la version précédente
1. corriger le test

[GitLab YAML Reference](https://docs.gitlab.com/ee/ci/yaml/README.html)

---

## DevOps

<div class="r-stack">

<span class="fragment current-visible">

Role

</span>

<span class="fragment">

❌ ~~Role~~ ⚠️

</span>

</div>

<div class="fragment">

DevOps ~ Agile

Mindset / grands principes

1. Communication
1. Travail collaboratif
1. Outil

[Agile Manifesto](https://agilemanifesto.org)

[12 Factor App](https://12factor.net)

</div>

---

## Cloture

* Questions ?
* Tour de table des attentes

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/initiation_ci_cd">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/initiation_ci_cd?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
