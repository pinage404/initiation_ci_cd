def hello(name):
    return f'Hello {name}'

def test_hello_world():
    assert hello('world') == 'Hello world'

def test_hello_toto():
    assert hello('toto') == 'Hello toto'
